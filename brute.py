#!/usr/bin/env python

import argparse
import sys
import os
import time

from dirutils import *

class Parsed:
    def __init__(self):
        self.mainset = list()
        self.subsets = list()

class Solution:
    def __init__(self):
        self.found = False

        self.subA = list()
        self.subB = list()

        self.partition_number = 0

class Result:
    def __init__(self):
        self.solution = Solution()
        self.time = 0.0

# get cmdline args
def parse_arguments():
    parser = argparse.ArgumentParser(description='Brute force solver for the Set Splitting problem')
    parser.add_argument('-f', '--file',
                        metavar='PATH',
                        help='if specified - read input from file in PATH, otherwise from STDIN',
                        nargs=1)
    parser.add_argument('-o', '--out',
                        metavar='PATH',
                        help='if specified - write a report file to PATH, otherwise to report.txt',
                        default=['report.txt'],
                        nargs=1)

    return parser.parse_args()

# parse input from file or STDIN
def parse_input(filename=None, reportpath='report.txt'):
    if filename is not None:
        inp = open(filename)
    else:
        inp = sys.stdin

    ret = Parsed()

    inp.readline()
    ret.mainset = list(map(int, inp.readline().split()))

    if len(ret.mainset) < 3:
        print('No solution exists for given case')
        gen_report(reportpath, Solution(), 0.0)
        sys.exit()

    i = 0
    for line in inp:
        if i % 2 == 1:
            templist = list(map(int, line.split()))

            if len(templist) < 2:
                print('No solution exists for given case')
                gen_report(reportpath, Solution(), 0.0)
                sys.exit()

            ret.subsets.append(templist)
        i += 1

    inp.close()

    return ret

# solve the problem for parsed data
def solve(parsed):
    # lists for storing temporary partition contents (as indices in main set)
    partA = list()
    partB = [0]

    # WARNING: integer field width limits main set size to 64 (32)
    n = len(parsed.mainset)
    count = 1
    limit = 1 << (n-1)
    skip = False
    #sol_found = False
    sol = Solution()
    while count < limit:
        # generate partitions by scanning a 2^(n-1)-field bit array
        el = 1
        for pos in range(n-1):
            if count & (1 << pos):
                partA.append(el)
            else:
                partB.append(el)
            el += 1

        subA = [ parsed.mainset[pos] for pos in partA ]
        subB = [ parsed.mainset[pos] for pos in partB ]

        for s in parsed.subsets:
            tmp = set(s)
            if set(subA) >= tmp or set(subB) >= tmp:
                skip = True
                break

        if not skip:
            sol.subA = subA
            sol.subB = subB
            sol.found = True
            sol.partition_number = count
            break

        skip = False
        partA = list()
        partB = [0]

        count += 1

    return sol

# measure execution time of a function
def measure(fn, data):
    res = Result()

    start = time.clock()
    res.solution = fn(data)
    end = time.clock()

    res.time = end - start

    return res

# generate a report and write it to [path]
def gen_report(path, solution, time):
    if os.path.dirname(path):
        mkdir_p(os.path.dirname(path))

    outfile = open(path, 'w')

    if solution.found:
        outfile.write('Status: OK\n')
    else:
        outfile.write('Status: ERR\n')

    outfile.write('Execution time: {:f}\n'.format(time))
    outfile.write('Partition number: {}\n'.format(solution.partition_number))

    outfile.close()

# main program
if __name__ == "__main__":
    args = parse_arguments()

    if args.file is not None:
        data = parse_input(args.file[0])
    else:
        data = parse_input(reportpath=args.out[0])

    result = measure(solve, data)

    if result.solution.found:
        print(*result.solution.subA)
        print(*result.solution.subB)

    else:
        print("No solution found for given case")

    gen_report(args.out[0], result.solution, result.time)
