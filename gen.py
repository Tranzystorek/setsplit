#!/usr/bin/env python

import argparse
import random
import os
import sys

from dirutils import *

def parse_arguments():
    parser = argparse.ArgumentParser(description='Generate test cases for SetSplitter')
    parser.add_argument('setsize',
                        metavar='N',
                        help='number of elements in the set',
                        nargs='?',
                        default=None,
                        type=int)
    parser.add_argument('nsubsets',
                        metavar='M',
                        help='number of subsets to generate',
                        nargs='?',
                        default=None,
                        type=int)
    parser.add_argument('-f', '--file',
                        metavar='PATH',
                        help='if specified - write generated test to PATH, otherwise write to STDOUT',
                        nargs=1)
    parser.add_argument('--help-format',
                        help='print test format and exit',
                        action='store_true',
                        dest='helpformat')

    return parser.parse_args()

def help_format():
    print("""\
N M
[MAIN SET OF SIZE N]
M TIMES:
>K
>[SUBSET OF SIZE K]\
""")

def generate(setsize, nsubs, filename=None):
    if filename is not None:
        if os.path.dirname(filename):
            mkdir_p(os.path.dirname(filename))
        out = open(filename, 'w')
    else:
        out = sys.stdout

    set = random.sample(range(1, 10 * setsize + 1), setsize)

    out.write('{} {}\n'.format(setsize, nsubs))
    out.write((' '.join(['{}'] * len(set)) + '\n').format(*set))

    for i in range(nsubs):
        #size = random.randint(2, args.setsize)
        size = round(random.gauss(setsize/2, setsize/2-3))
        if size >= setsize:
            size = setsize - 1
        elif size <= 1:
            size = 2
        m = random.sample(set, size)
        out.write('{}\n'.format(size))
        out.write((' '.join(['{}'] * len(m)) + '\n').format(*m))

    out.close()

if __name__ == "__main__":
    args = parse_arguments()

    if args.helpformat:
        help_format()
        sys.exit()

    if not args.setsize or not args.nsubsets:
        print('You must specify arguments M, N.')
        sys.exit()

    if args.file is not None:
        generate(args.setsize, args.nsubsets, args.file[0])
    else:
        generate(args.setsize, args.nsubsets)
